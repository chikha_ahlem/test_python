#!/usr/bin/python
# Function to check if n is a multiple of 3 on not
def CheckMultiple(n): 
    number1 = 0
    number2 = 0
  
    if(n < 0):  
        n = -n 
    if(n == 0): 
        return True
    if(n == 1):  
        return False
  
    while(n): 
          
        if(n & 1):  
            number1 += 1
  
        if(n & 2): 
            number2 += 1
        n = n >> 2
  
    return CheckMultiple(abs(number1 - number2)) 
  
# Program to test function CheckMultiple  
number = 24
if (CheckMultiple(number)):  
    print(number, 'est divisbile par 3') 
else: 
    print(number, "n'est pas divisble par 3") 
  
