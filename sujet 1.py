#!/usr/bin/python

# Function definition is here
def majuscule(chaine):

    i = 0
 
    while (chaine[i] != '\0'): #Tant que nous ne sommes pas arrivés a la fin de la chaine, continuer
        #si chaine[1] est une minuscule, Car les minuscules se situent entre 97 et 122 inclusivement
        if (chaine[i]  >= 97 and chaine[i] <= 122): 
            #/Convertir en majuscules
             chaine[i] = chaine[i] - 32
        i += 1
    
    return chaine